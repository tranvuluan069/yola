const io = require('socket.io')(1234, {
    cors: {
        origin: 'http://localhost:3000' 
    }
})

let users = [];

let usersServer = [];

const addUser = (userId, socketId) => {
  !users.some((user) => user.userId === userId) &&
    users.push({ userId, socketId });
};

//test
const clientJoinSocketServer = (userId, socketId) => {
  !usersServer.some((user) => user.userId === userId) && usersServer.push({ userId, socketId });
}

const removeUser = (socketId) => {
  users = users.filter((user) => user.socketId !== socketId);
};

const removeUserInServer = (socketId) => {
  usersServer = usersServer.filter((user) => user.socketId !== socketId);
}


const getUser = (userId) => {
  return usersServer.find((user) => user.userId === userId); 
};
 
io.on("connection", (socket) => {
  //when ceonnect
  console.log("a user connected id: " + socket.id);

  socket.on('client_join_socketserver', (data) => {
    clientJoinSocketServer(data.id, socket.id);
    console.log('users in server: ', usersServer);
  })

  //take userId and socketId from user
  socket.on("addUser", (userId) => {
    addUser(userId, socket.id);
    io.emit("getUsers", users);
  });

  // server receive message from client and send message to another client
  socket.on("client_send_message", (data) => {
    console.log('server recieve message: ', data);
    // const user = getUser(data.recipient);
    data.recipients.map(recipient => {
      const user = getUser(recipient.uid);
      console.log(user);
      if (user) {
        io.to(user.socketId).emit("server_send_message", data);
      }
    })
  });

  // server receive signal addfriend
  socket.on("client_add_friend", (data) => {
    const user = getUser(data.userId);
    // console.log('end user', data);
    // io.to(user.socketId).emit("server_add_friend", data);
  });


  console.log('users in server: ');
  console.log(usersServer);

  //when disconnect
  socket.on("disconnect", () => {
    console.log("a user disconnected!");
    removeUser(socket.id);
    removeUserInServer(socket.id);
    io.emit("getUsers", users);
  });
});
