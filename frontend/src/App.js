import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Login from './pages/Auth/Login';
import Main from './pages/Main';
import Register from './pages/Auth/Register';
import Auth404 from './pages/Auth/Auth-404';
import AuthRecoverPassword from './pages/Auth/Auth-Recover-PW';
import RequireAuth from './services/Auth/RequireAuth';


function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
          <Route path="/" element={
            <RequireAuth>
              <Main />
            </RequireAuth>
          } />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/auth404" element={<Auth404 />} />
          <Route path="/auth-recover-pw" element={<AuthRecoverPassword />} />
          <Route path=":accountId" element={<Main />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
