import React from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, Navigate } from 'react-router-dom';


export default function RequireAuth(props) {
    const auth = useSelector(state => state.auth);
    console.log(auth);
    const location = useLocation();
    const dispatch = useDispatch();

    return (
        auth.account ? React.Children.only(props.children) : <Navigate to="/login" state={{ from: location }} replace />
    )
}
