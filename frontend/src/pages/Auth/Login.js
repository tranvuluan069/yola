import { Link } from 'react-router-dom';
import { useState } from 'react';
import { getSmsOTP } from './../../apis/authApi';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import AuthSpinner from './../../components/spinners/authSpinner';
import Alert from './../../components/alerts/Alert';
import Step2 from '../../components/Auth/stepLogin';

export default function Login() {
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const auth = useSelector(state => state.auth);

    const handleSubmit = (e) => {
        e.preventDefault();
        const user = {
            username: username,
            password: password
        }
        getSmsOTP(user, dispatch);
    }

    return (
        <>
            <div id="body" className="auth-page" >
                {/* Log In page */}
                <div className="container-md">
                    <div className="row vh-100 d-flex justify-content-center">
                        <div className="col-12 align-self-center">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-lg-4 mx-auto">
                                        {(auth.login.step === 1) && (
                                            <div className="card">
                                                <div className="card-body p-0 auth-header-box">
                                                    <div className="text-center p-3">
                                                        <Link to="/" className="logo logo-admin">
                                                            {/* <img src="assets/images/logo-sm.png" height={50} alt="logo" className="auth-logo" /> */}
                                                        </Link>
                                                        <h4 className="mt-3 mb-1 fw-semibold text-white font-18">YOLA Hi!</h4>
                                                        <p className="text-muted  mb-0">Sign in to Yola</p>
                                                    </div>
                                                </div>
                                                <div className="card-body pt-0">
                                                    {
                                                        (auth.login.error) ? (<Alert message="Lỗi!" />) : ''
                                                    }

                                                    <form className="my-4" onSubmit={handleSubmit}>
                                                        <div className="form-group mb-2">
                                                            <label className="form-label">Username</label>
                                                            <input onChange={(event) => setUserName(event.target.value)} type="text" className="form-control" placeholder="Enter username" />
                                                        </div>
                                                        <div className="form-group">
                                                            <label className="form-label">Password</label>
                                                            <input onChange={(event) => setPassword(event.target.value)} type="password" className="form-control" placeholder="Enter password" />
                                                        </div>
                                                        <div className="form-group row mt-3">
                                                            <div className="col-sm-6">
                                                                <div className="form-check form-switch form-switch-success">
                                                                    <input className="form-check-input" type="checkbox" id="customSwitchSuccess" />
                                                                    <label className="form-check-label" htmlFor="customSwitchSuccess">Remember me</label>
                                                                </div>
                                                            </div>{/*end col*/}
                                                            <div className="col-sm-6 text-end">
                                                                <Link to="/auth-recover-pw" className="text-muted font-13"><i className="dripicons-lock" /> Forgot password?</Link>
                                                            </div>{/*end col*/}
                                                        </div>{/*end form-group*/}
                                                        <div className="form-group mb-0 row">
                                                            <div className="col-12">
                                                                <div className="d-grid mt-3">
                                                                    <button className="btn btn-primary" type="submit">
                                                                        Log In
                                                                        <i className="fa-solid fa-arrow-right-to-bracket mx-2" />
                                                                    </button>
                                                                </div>
                                                            </div>{/*end col*/}
                                                        </div> {/*end form-group*/}
                                                    </form>{/*end form*/}
                                                    <div className="m-3 text-center text-muted">
                                                        <p className="mb-0">Don't have an account ?  <Link to="/register" className="text-primary ms-2">Free Resister</Link></p>
                                                    </div>
                                                    <hr className="hr-dashed mt-4" />
                                                    <div className="text-center mt-n5">
                                                        <h6 className="card-bg px-3 my-4 d-inline-block">Or Login With</h6>
                                                    </div>
                                                    <div className="d-flex justify-content-center mb-1">
                                                        <a className="d-flex justify-content-center align-items-center thumb-sm bg-soft-primary rounded-circle me-2">
                                                            <i className="fab fa-facebook align-self-center" />
                                                        </a>
                                                        <a className="d-flex justify-content-center align-items-center thumb-sm bg-soft-info rounded-circle me-2">
                                                            <i className="fab fa-twitter align-self-center" />
                                                        </a>
                                                        <a className="d-flex justify-content-center align-items-center thumb-sm bg-soft-danger rounded-circle">
                                                            <i className="fab fa-google align-self-center" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>)
                                        }
                                        {(auth.login.step === 2) && <Step2 username={username} password={password} />}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {(auth.login.isFetching || auth.auth2F.isFetching) && <AuthSpinner />}
        </>
    )
}


