import { Link } from 'react-router-dom';
import { useState } from 'react';
import { setStepForgot, setUserForgot } from '../../redux/authSlice';
import { useDispatch, useSelector } from 'react-redux';
import { forgotPasswordSendOTP, verifyForgotPasswordOTP, resetForgotPassword } from '../../apis/authApi';
import {useNavigate} from 'react-router-dom';

export default function AuthRecoverPassword() {
    const step = useSelector(state => state.auth.forgotPassword.step);


    const loadStep = () => {
        if (step == 1)
            return  <Step1 />
        else if (step == 2) return  <Step2 />
        else if (step == 3) return  <Step3 />
    }

    return (
        <div className="container">
            <div className="row vh-100 d-flex justify-content-center">
                <div className="col-12 align-self-center">
                    <div className="row">
                        <div className="col-lg-5 mx-auto">
                            {loadStep()}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

function Step1(props) {
    const dispatch = useDispatch();
    const [phone, setPhone] = useState();
    const [username, setUsername] = useState();

    const handleReset = async () => {
        const userOTP = {
            username: username,
            phone: phone,
        }
        console.log(userOTP);
        let sendOTP = await forgotPasswordSendOTP(userOTP);
        if (sendOTP != false) {
            dispatch(setStepForgot({ step: 2 }));
            dispatch(setUserForgot({ username: username, phone: phone }));
        } else {
            alert('User không tồn tại!');
        }
    }

    return (
        <>
            <div className="card">
                <div className="card-body p-0 auth-header-box">
                    <div className="text-center p-3">
                        <a href="index.html" className="logo logo-admin">
                            YOLA HI !
                        </a>
                        <h4 className="mt-3 mb-1 fw-semibold text-white font-18">Reset Password For Unikit</h4>
                        <p className="text-muted  mb-0">Enter your phone and instructions will be sent to you!</p>
                    </div>
                </div>
                <div className="card-body pt-0">
                    <form className="my-4" action="index.html">
                        <div className="form-group mb-3">
                            <label className="form-label" htmlFor="username">Username</label>
                            <input type="text" className="form-control" onChange={(event) => setUsername(event.target.value)} id="userEmail" name="phone" placeholder="Enter your phone" />
                        </div>
                        <div className="form-group mb-3">
                            <label className="form-label" htmlFor="username">Phone</label>
                            <input type="text" className="form-control" onChange={(event) => setPhone(event.target.value)} id="userEmail" name="phone" placeholder="Enter your phone" />
                        </div>
                        <div className="form-group mb-0 row">
                            <div className="col-12">
                                <button onClick={handleReset} className="btn btn-primary w-100" type="button">Reset <i className="fas fa-sign-in-alt ms-1" /></button>
                            </div>
                        </div>
                    </form>
                    <div className="text-center text-muted">
                        <p className="mb-1">Remember It ?  <Link to="/register" className="text-primary ms-2">Sign in here</Link></p>
                    </div>
                </div>
                <div className="card-body bg-light-alt text-center">
                    ©  SGU <a href="fb.com/luan.luan23082001"> Culi </a>
                </div>
            </div>
        </>
    )
}


function Step2(props) {
    const dispatch = useDispatch();
    const forgotPasswordStore = useSelector(state => state.auth.forgotPassword);
    const [otp, setOTP] = useState();
    const handleBack = () => {
        dispatch(setStepForgot({ step: 1 }));
    }

    const handleConfirm = async () => {
        const user = {
            username: forgotPasswordStore.username,
            phone: forgotPasswordStore.phone,
            otp: otp,
            exp: Date.now(),
        }
        let verifySMSOTP = await verifyForgotPasswordOTP(user);
        if (verifySMSOTP != false) {
            dispatch(setStepForgot({ step: 3 }));
        } else {
            alert(verifySMSOTP.data);
        }
        console.log(verifySMSOTP);
    }

    return (
        <>
            <div className="card">
                <div className="card-body p-0 auth-header-box">
                    <div className="text-center p-3">
                        <Link to="/" className="logo logo-admin">
                            <img src="https://mannatthemes.com/unikit/default/assets/images/logo-sm.png" height={50} alt="logo" className="auth-logo" />
                        </Link>
                        <h4 className="mt-3 mb-1 fw-semibold text-white font-18">Let's Get Started Yola</h4>
                        <p className="text-muted  mb-0">Verify Yola</p>
                    </div>
                </div>
                <div className="m-3 text-center text-muted">
                    <form >
                        <div className="form-group row" >
                            <label className="col-4 form-label">Enter OTP</label>
                            <div className="col-7">
                                <input onChange={(event) => setOTP(event.target.value)} type="text" className="form-control" />
                            </div>
                        </div>
                        {/* {auth.auth2F.error && (<span>OTP invalid</span>)} */}
                    </form>
                </div>
                <div className="m-3 text-center text-muted d-flex justify-content-between">
                    <p onClick={handleBack} className="mb-0 btn btn-outline-primary">Back</p>
                    <p onClick={handleConfirm} className="mb-0 btn btn-primary">Confirm</p>
                </div>
            </div>
        </>
    )
}


function Step3() {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [password, setPassword] = useState();
    const [renewPassword, setRenewPassword] = useState();
    const forgotPasswordStore = useSelector(state => state.auth.forgotPassword);

    const handleBack = () => {
        dispatch(setStepForgot({ step: 1 }));
    }

    const handleReset = async () => {
        if (password === renewPassword) {
            let user = {
                username: forgotPasswordStore.username,
                password: password
            }
            let resetPassword =  await resetForgotPassword(user);
            if (resetPassword != false) {
                alert(resetPassword.data.message);
                navigate('/login');
                
            } else {
                alert('Lỗi!');
            }
        }
    }

    return (
        <>
            <div className="card">
                <div className="card-body p-0 auth-header-box">
                    <div className="text-center p-3">
                        <Link to="/" className="logo logo-admin">
                            <img src="https://mannatthemes.com/unikit/default/assets/images/logo-sm.png" height={50} alt="logo" className="auth-logo" />
                        </Link>
                        <h4 className="mt-3 mb-1 fw-semibold text-white font-18">Let's Get Started Yola</h4>
                        <p className="text-muted  mb-0">Verify Yola</p>
                    </div>
                </div>
                <div className="m-3 text-center text-muted">
                    <form >
                        <div className="form-group row" >
                            <label className="col-4 form-label">New password</label>
                            <div className="col-7">
                                <input onChange={(event) => setPassword(event.target.value)} type="text" className="form-control" />
                            </div>
                        </div>
                        <div className="form-group row" >
                            <label className="col-4 form-label">Renew password</label>
                            <div className="col-7">
                                <input onChange={(event) => setRenewPassword(event.target.value)} type="text" className="form-control" />
                            </div>
                        </div>
                        {/* {auth.auth2F.error && (<span>OTP invalid</span>)} */}
                    </form>
                </div>
                <div className="m-3 text-center text-muted d-flex justify-content-between">
                    <p onClick={handleBack} className="mb-0 btn btn-outline-primary">Back</p>
                    <p onClick={handleReset} className="mb-0 btn btn-primary">Reset</p>
                </div>
            </div>
        </>
    )
}