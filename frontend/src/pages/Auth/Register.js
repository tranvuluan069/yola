import { Step1, Step2 } from './../../components/Auth/stepRegister';
import { useSelector, useDispatch } from 'react-redux';
import AuthSpinner from './../../components/spinners/authSpinner';


export default function Register() {

  const auth = useSelector(state => state.auth);



  return (
    <div id="body" className="auth-page" >
      {/* Log In page */}
      <div className="container-md">
        <div className="row vh-100 d-flex justify-content-center">
          <div className="col-12 align-self-center">
            <div className="card-body">
              <div className="row">
                <div className="col-lg-4 mx-auto">
                  {(auth.register.step === 1) && <Step1 />}
                  {(auth.register.step === 2) && <Step2 />}
                </div>{/*end col*/}
              </div>{/*end row*/}
            </div>{/*end card-body*/}
          </div>{/*end col*/}
        </div>{/*end row*/}
      </div>
      {console.log('render register')}
      {(auth.register.isFetching) && <AuthSpinner />}
    </div>
  )
}





