import Nav from "../components/Navbar/Nav";
import Footer from './../components/Footer/Footer';
import TitleMenu from './../components/ContentMenu/TitleMenu';
import Search from './../components/ContentMenu/Conversations/Search';
import ConversationList from "./../components/ContentMenu/Conversations/ConversationList";
import ContactList from './../components/ContentMenu/Contacts/ContactList';
import AuthSpinner from './../components/spinners/authSpinner';
import Chat from './../components/Chats/Chat';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import { useEffect, useState, useRef } from 'react';
import { io } from 'socket.io-client';


export default function Main() {
    const auth = useSelector(state => state.auth);
    const location = useLocation();
    const navigate = useNavigate();
    const [isChooseContact, setIsChooseContact] = useState(false);
    const [isChooseChat, setIsChooseChat] = useState(false);
    const conversation = useSelector(state => state.conversation);
    const dispatch = useDispatch();
    // const socket = useRef();
    const [socket, setSocket] = useState();

    useEffect(() => {
        if (!auth.account) {
            navigate('/login');
            window.location.reload();
        }

        if (location.pathname === '/contact') {
            setIsChooseContact(true);
            setIsChooseChat(false);
        } else if (location.pathname === '/chat') {
            setIsChooseContact(false);
            setIsChooseChat(true);
        } else {
            setIsChooseContact(false);
            setIsChooseChat(true);
        }

    }, [location, auth.account]);

    useEffect(() => {
        // let socio = io('http://14.225.192.186:8000', {
        //     withCredentials: true,
        //     extraHeaders: {
        //         "my-custom-header": "abcd"
        //     }
        // });
        if (auth.account && !socket) {
            let socio = io('http://localhost:1234');
            setSocket(socio);
        }

    }, []);

    useEffect(() => {
        auth.account && socket && socket.emit('client_join_socketserver', { id: auth.account._id, fullname: auth.account.fullname });
    })

    return (
        auth.account ? (<div>
            <Nav />
            <div className="page-wrapper">
                <div className="page-content-tab">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                <div className="chat-box-left">
                                    {/* Title Nav */}
                                    {isChooseChat && (<TitleMenu title={'Chat'} />)}
                                    {isChooseContact && (<TitleMenu title={'Contact'} />)}
                                    {/*END Title Nav*/}

                                    {/* Search */}
                                    <Search socket={socket} />
                                    {/*END Search*/}
                                    {/* Content menu  */}
                                    {/* {isChooseChat && (<ConversationList />)} */}
                                    {isChooseContact && (<ContactList />)}
                                    {/* END Content menu  */}


                                </div>

                                <div className="chat-box-right">
                                    {/* CHAT */ }
                                    {conversation.conversation ? (<Chat socket={socket} />) : ''}
                                    {/* END CHAT */}
                                </div>
                                {/*end chat-box-right */}
                            </div>
                        </div>
                    </div>
                    {/* Footer  */}
                    <Footer />
                    {/* END Footer */}
                </div>
                {/* end page content */}
            </div>
            {auth.logout.isFetching && <AuthSpinner />}
        </div>
        ) : <></>
    )
}