import {encrypt, decrypt} from './ase';
const NodeRSA = require('node-rsa');


export const createRSA = (secretKeyForASE) => {
    const rsa = new NodeRSA({b: 512});

    const publicKey = rsa.exportKey('public');
    const privateKey = rsa.exportKey('private');

    // encrypt private key
    const cipherText = encrypt(secretKeyForASE, privateKey);

    const RSA = {
        publicKey: publicKey,
        privateKey: cipherText
    }

    return RSA;
}

export const decryptPrivateKey = (secretKeyForASE, cipherText) => {
    const privateKey = decrypt(secretKeyForASE, cipherText);
    return privateKey;
}