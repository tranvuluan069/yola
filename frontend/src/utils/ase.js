var CryptoJS = require("crypto-js");

export const encrypt = (secretKey, value) => {
    var ciphertext = CryptoJS.AES.encrypt(value, secretKey).toString();
    return ciphertext;
}

export const decrypt = (secretKey, cipherText) => {
    var bytes = CryptoJS.AES.decrypt(cipherText, secretKey);
    var plaintext = bytes.toString(CryptoJS.enc.Utf8);
    return plaintext;
}