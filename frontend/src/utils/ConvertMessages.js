// import RSAKey from 'react-native-rsa';
import NodeRSA from 'node-rsa';

export const encryptMessage = (message, members) => {
    const arrEncryptObject = [];
    members.map(member => {
        const key = new NodeRSA(member.publicKey);
        // key.setPublicString(member.pub);

        const encrypted = key.encrypt(message, 'base64');
        const encryptObject = {
            uid: member.uid,
            encryptMessage:  encrypted
        }
        arrEncryptObject.push(encryptObject);
    });
    return arrEncryptObject;
}



export const decryptMessage = (message, privateKey) => {
    const key = new NodeRSA(privateKey);
    const decryptMessage = key.decrypt(message, 'utf-8');
    return decryptMessage;

}