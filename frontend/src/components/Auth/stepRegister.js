import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import { stepRegister } from './../../redux/authSlice';
import { registerStep1, registerStep2 } from './../../apis/authApi';
import Joi from 'joi';

export const Step1 = (props) => {
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();
    const [confirmPassword, setConfirmPassword] = useState();
    const [email, setEmail] = useState();
    const [phone, setPhone] = useState();
    const [fullname, setFullname] = useState();
    const dispatch = useDispatch();
    const auth = useSelector(state => state.auth);

    const schema = Joi.object({
        username: Joi.string()
            .min(1)
            .required(),

        password: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
        email: Joi.string()
            .min(1)
            .required(),
        phone: Joi.string()
            .min(1)
            .required(),
        fullname: Joi.string()
            .min(1)
            .required(),
        confirmPassword: Joi.ref('password')
    })
        .with('password', 'confirmPassword');



    const validationForm = async () => {
        try {
            const value = await schema.validateAsync({ username: username, password: password, confirmPassword: confirmPassword,
                phone: phone, fullname: fullname, email: email
            });
        }
        catch (err) {
            console.log(err);
            alert(err.message);
            return;
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        validationForm();

        const user = {
            fullname: fullname,
            username: username,
            password: password,
            phone: phone,
            email: email,
            image: 'https://ui-avatars.com/api/?background=random&name=' + fullname
        }

        registerStep1(user, dispatch);
    }

    return (
        <div className="card">
            <div className="card-body p-0 auth-header-box">
                <div className="text-center p-3">
                    <Link to="/" className="logo logo-admin">
                        {/* <img src="https://mannatthemes.com/unikit/default/assets/images/logo-sm.png" height={50} alt="logo" className="auth-logo" /> */}
                    </Link>
                    <h4 className="mt-3 mb-1 fw-semibold text-white font-18">Let's Get Started YOLA</h4>
                    <p className="text-muted  mb-0">Sign up to continue to YOLA.</p>
                </div>
            </div>
            <div className="card-body pt-0">
                {auth.register.error && (
                    <div className="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>{auth.register.message}</strong>
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                )}
                <form onSubmit={handleSubmit} className="my-4">
                    <div className="form-group mb-2">
                        <label className="form-label" htmlFor="username">Fullname</label>
                        <input onChange={(event) => setFullname(event.target.value)} type="text" className="form-control" id="fullname" name="username" placeholder="Enter fullname" />
                    </div>
                    <div className="form-group mb-2">
                        <label className="form-label" htmlFor="username">Username</label>
                        <input onChange={(event) => setUserName(event.target.value)} type="text" className="form-control" id="username" name="username" placeholder="Enter username" />
                    </div>
                    <div className="form-group mb-2">
                        <label className="form-label" htmlFor="useremail">Email</label>
                        <input onChange={(event) => setEmail(event.target.value)} type="email" className="form-control" id="useremail" name="user email" placeholder="Enter email" />
                    </div>
                    <div className="form-group mb-2">
                        <label className="form-label" htmlFor="userpassword">Password</label>
                        <input onChange={(event) => setPassword(event.target.value)} type="password" className="form-control" name="password" id="userpassword" placeholder="Enter password" />
                    </div>
                    <div className="form-group mb-2">
                        <label className="form-label" htmlFor="Confirmpassword">ConfirmPassword</label>
                        <input onChange={(event) => setConfirmPassword(event.target.value)} type="password" className="form-control" name="password" id="Confirmpassword" placeholder="Enter Confirm password" />
                    </div>
                    <div className="form-group mb-2">
                        <label className="form-label" htmlFor="mobileNo">Mobile Number</label>
                        <input onChange={(event) => setPhone(event.target.value)} type="text" className="form-control" id="mobileNo" name="mobile number" placeholder="Enter Mobile Number" />
                    </div>
                    <div className="form-group row mt-3">
                        <div className="col-12">
                            <div className="form-check form-switch form-switch-success">
                                <input className="form-check-input" type="checkbox" id="customSwitchSuccess" />
                                <label className="form-check-label" htmlFor="customSwitchSuccess">By registering you agree to the Unikit <Link to="/" className="text-primary">Terms of Use</Link></label>
                            </div>
                        </div>{/*end col*/}
                    </div>
                    <div className="form-group mb-0 row">
                        <div className="col-12">
                            <div className="d-grid mt-3">
                                <button className="btn btn-primary" type="submit">Register <i className="fas fa-sign-in-alt ms-1" /></button>
                            </div>
                        </div>{/*end col*/}
                    </div>
                </form>
                <div className="m-3 text-center text-muted">
                    <p className="mb-0">Already have an account ? <Link to="/login" className="text-primary ms-2">Log in</Link></p>
                </div>
            </div>
        </div>
    )
}


export const Step2 = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const auth = useSelector(state => state.auth);
    const [OTP, setOTP] = useState();

    const handleConfirm = (e) => {
        const userOTP = {
            ...auth.register.user,
            otp: OTP,
            exp: Date.now()
        }
        console.log(userOTP);
        registerStep2(userOTP, dispatch);
    }

    const handleBack = (e) => {
        dispatch(stepRegister({ step: 1 }));
        navigate('/register');
    }


    return (
        <>
            <div className="card">
                <div className="card-body p-0 auth-header-box">
                    <div className="text-center p-3">
                        <Link to="/" className="logo logo-admin">
                            <img src="https://mannatthemes.com/unikit/default/assets/images/logo-sm.png" height={50} alt="logo" className="auth-logo" />
                        </Link>
                        <h4 className="mt-3 mb-1 fw-semibold text-white font-18">Let's Get Started Yola</h4>
                        <p className="text-muted  mb-0">Register Yola</p>
                    </div>
                </div>
                <div className="m-3 text-center text-muted">
                    <form >
                        <div className="form-group row" >
                            <label className="col-4 form-label">Enter OTP</label>
                            <div className="col-7">
                                <input onChange={(event) => setOTP(event.target.value)} type="text" className="form-control" />
                            </div>
                        </div>
                        {auth.register.error && (<span>OTP invalid</span>)}
                    </form>
                </div>
                <div className="m-3 text-center text-muted d-flex justify-content-between">
                    <p onClick={handleBack} className="mb-0 btn btn-outline-primary">Back</p>
                    <p onClick={handleConfirm} className="mb-0 btn btn-primary">Confirm</p>
                </div>
            </div>
        </>
    )
}

