import {useDispatch, useSelector} from 'react-redux';
import { stepLogin } from './../../redux/authSlice';
import { verifySmsOTP } from './../../apis/authApi';
import {useNavigate, Link} from 'react-router-dom';
import {useState} from 'react';


export default function Step2 (props) {
    const {username, password} = props;
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const auth = useSelector(state => state.auth);
    const [OTP, setOTP] = useState();

    const handleConfirm = (e) => {
        const userOTP = {
            username: username,
            password: password,
            otp: OTP,
            exp: Date.now()
        }

        verifySmsOTP(userOTP, dispatch, navigate);

    }

    const handleBack = () => {
        dispatch(stepLogin({ step: 1}));
    }

    return (
        <>
            <div className="card">
                <div className="card-body p-0 auth-header-box">
                    <div className="text-center p-3">
                        <Link to="/" className="logo logo-admin">
                            <img src="https://mannatthemes.com/unikit/default/assets/images/logo-sm.png" height={50} alt="logo" className="auth-logo" />
                        </Link>
                        <h4 className="mt-3 mb-1 fw-semibold text-white font-18">Let's Get Started Yola</h4>
                        <p className="text-muted  mb-0">Verify Yola</p>
                    </div>
                </div>
                <div className="m-3 text-center text-muted">
                    <form >
                        <div className="form-group row" >
                            <label className="col-4 form-label">Enter OTP</label>
                            <div className="col-7">
                                <input onChange={(event) => setOTP(event.target.value)} type="text" className="form-control" />
                            </div>
                        </div>
                        {auth.auth2F.error && (<span>OTP invalid</span>)}
                    </form>
                </div>
                <div className="m-3 text-center text-muted d-flex justify-content-between">
                    <p onClick={handleBack} className="mb-0 btn btn-outline-primary">Back</p>
                    <p onClick={handleConfirm} className="mb-0 btn btn-primary">Confirm</p>
                </div>
            </div>
        </>
    )
}