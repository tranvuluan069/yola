
import { Link } from 'react-router-dom';
import { logout } from './../../apis/authApi';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

export default function Nav() {
    const auth = useSelector(state => state.auth);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleLogout = () => {
        logout(dispatch, navigate);
    }

    return (
        <div>
            <div className="left-sidebar">
                {/* LOGO */}
                <div className="brand">
                    <Link to="/" className="logo">
                        <span>
                            {/* <img src={logoSm} alt="logo-small" className="logo-sm" /> */}
                        </span>
                        <span>
                            Stupid Chat
                            {/* <img src={logo} alt="logo-large" className="logo-lg logo-light" /> */}
                            {/* <img src={logoDark} alt="logo-large" className="logo-lg logo-dark" /> */}
                        </span>
                    </Link>
                </div>
                {/* Info */}
                <div className="sidebar-user-pro media border-end">
                    <div className="position-relative mx-auto">
                        <img src={auth.account.image} alt="user" className="rounded-circle thumb-md" />
                        <span className="online-icon position-absolute end-0"><i className="mdi mdi-record text-success" /></span>
                    </div>
                    <div className="media-body ms-2 user-detail align-self-center">
                        <h5 className="font-14 m-0 fw-bold">{auth.account.fullname}</h5>
                        <p className="opacity-50 mb-0">{auth.account.email}</p>
                    </div>
                </div>
                {/* Navigation */}
                <div className="menu-content h-100" data-simplebar>
                    <div className="menu-body navbar-vertical">
                        <div className="collapse navbar-collapse tab-content" id="sidebarCollapse">
                            <ul className="navbar-nav tab-pane active" id="Main" role="tabpanel">
                                {/* <li className="menu-label mt-0 text-primary font-12 fw-semibold cursor-pointer">
                                    <Link to="/profile" >
                                        <i className="fa fa-user icon-nav"></i>
                                        <span className="title-nav" >Profile</span>
                                    </Link>
                                </li> */}
                                {/* <li className="menu-label mt-0 text-primary font-12 fw-semibold cursor-pointer">
                                    <Link to="/chat">
                                        <i className="fa fa-comment icon-nav"> </i>
                                        <span className="title-nav" >Chat</span>
                                    </Link>
                                </li> */}
                                <li className="menu-label mt-0 text-primary font-12 fw-semibold cursor-pointer">
                                    <Link to="/contact" >
                                        <i className="fa fa-address-book icon-nav"> </i>
                                        <span className="title-nav" >Contact</span>
                                    </Link>
                                </li>
                                {/* <li className="menu-label mt-0 text-primary font-12 fw-semibold cursor-pointer">
                                    <i className="fa fa-cog icon-nav"> </i>
                                    <span className="title-nav" >Settings</span>
                                </li> */}
                                <li onClick={handleLogout} className="menu-label mt-0 text-primary font-12 fw-semibold cursor-pointer">
                                    <i className="fa fa-sign-out icon-nav"> </i>
                                    <span className="title-nav" >LOGOUT</span>
                                </li>
                            </ul>
                            {/*end nav*/}
                        </div>
                        {/*end sidebarAuthentication*/}
                        {/*end nav-item*/}
                    </div>
                </div>
            </div>
        </div>
    )
}
