
export default function TitleMenu(props) {
    return (
        <div className="chat-search mb-3">
            <h4> {props.title} </h4>
        </div>
    )
}