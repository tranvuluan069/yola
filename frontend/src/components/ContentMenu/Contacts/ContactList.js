import ContactItem from './ContactItem';
import Spinner from './../../spinners/spinner';
import { useSelector, useDispatch } from 'react-redux';
import { useEffect, useState } from 'react';
import {getContacts} from './../../../apis/contactApi';

export default function ContactList() {
    const contact = useSelector(state => state.contact);
    const [contactList, setContactList] = useState();
    const dispatch = useDispatch();
    const auth = useSelector(state => state.auth);

    useEffect(() => {
        getContacts(auth.account, dispatch, auth.accessToken);
    }, []);

    return (
        <div>
            <div className="chat-body-left" data-simplebar>
                <div className="tab-content chat-list" id="pills-tabContent">
                    <div className="tab-pane fade show active" id="general_chat">
                        {/* contact item */}
                        {
                            contact.contacts?.map((value, key) => {
                                return <ContactItem key={key} user={value} />
                            })
                        }
                        {/* contact item */}
                    </div>
                </div>
                { contact.getContacts.isFetching && <Spinner /> }
            </div>
        </div>
    )
}