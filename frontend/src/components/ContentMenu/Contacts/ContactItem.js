import { useSelector, useDispatch } from 'react-redux';
import { getPrivateConversation } from '../../../apis/conversationApi';

export default function ContactItem(props) {
    const { user } = props;
    const dispatch = useDispatch();
    const conversation = useSelector(state => state.conversation);
    const account = useSelector(state => state.auth);

    const handleChooseContact = () => {
        const arrayMemberId = [
            user._id, account.account._id
        ]
        getPrivateConversation(arrayMemberId, dispatch);
    }

    return (
        <div onClick={handleChooseContact} className="media new-message">
            <div className="media-left">
                <img src={user.image} alt="user" className="rounded-circle thumb-md" />
                <span className="round-10 bg-success" />
            </div>{/* media-left */}
            <div className="media-body">
                <div className="d-inline-block">
                    <h6>{user.fullname}</h6>
                </div>
            </div>
        </div>
    )
}