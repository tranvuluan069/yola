import { useState } from 'react';
import { searchContacts, addContact } from './../../../apis/contactApi';
import { useSelector, useDispatch } from 'react-redux';

export default function Search(props) {
    const socket = props.socket;
    const contacts = useSelector(state => state.contact.contacts);
    const [searchText, setSearchText] = useState();
    const [isLoading, setIsLoading] = useState(false);
    const [newContacts, setNewContacts] = useState(null);

    const handleSearch = async (event) => {
        setSearchText(event.target.value);
        setIsLoading(true);
        const allUsers = await searchContacts(searchText, setIsLoading);
        const newUsers = [];
        allUsers.forEach(user => { 
            if (!contacts.find(item => item._id === user._id)) {
                newUsers.push(user);
            }
        });
        setNewContacts(newUsers);
    }

    const handleBlur = (event) => {
        // console.log('event.relatedTarget', event.relatedTarget);
        if (!event.currentTarget.contains(event.relatedTarget)) {
            // console.log('blur');
            setIsLoading(false);
            setNewContacts(null);
        }
    }

    return (
        <div onBlur={handleBlur} >
            <div className="chat-search mb-3">
                <div className="form-group">
                    <div className="input-group">
                        <input onChange={handleSearch} type="text" id="chat-search" name="chat-search" className="form-control" placeholder="Search new friends" autoComplete="off" />
                    </div>
                </div>
                {isLoading && <LoadingContacts />}
                {!isLoading && newContacts && <LoadedContacts newContacts={newContacts} socket={socket} />}
            </div>
        </div>
    )
}

export function LoadingContacts() {
    return (
        <ul className="list-group py-3">
            <div className="mt-3 spinner-border text-primary justify-content-center align-self-center" role="status">
            </div>
        </ul>
    )
}

export function LoadedContacts(props) {
    const socket = props.socket;
    const { newContacts } = props;
    const currentAccount = useSelector(state => state.auth.account);
    const dispatch = useDispatch();

    const handleAddFriend = (user) => {
        addContact({
            currentAccount: currentAccount,
            accountIdToAdd: user
        })

        socket && socket.emit('client_add_friend', {
            sender: currentAccount._id,
            recipient: user._id
        })
    }

    return (
        <ul className="list-group">
            {
                newContacts.map((user, index) =>
                (<li key={index} className="list-group-item d-flex justify-content-between">
                    <div>{user.fullname}</div>
                    <button onClick={() => handleAddFriend(user)}>+</button>
                </li>))
            }
        </ul>
    )
}