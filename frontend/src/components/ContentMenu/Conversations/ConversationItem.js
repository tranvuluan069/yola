
export default function ConversationItem() {

    return (
        <a className="media new-message">
            <div className="media-left">
                <img src="https://mannatthemes.com/unikit/default/assets/images/users/user-4.jpg" alt="user" className="rounded-circle thumb-md" />
                <span className="round-10 bg-success" />
            </div>{/* media-left */}
            <div className="media-body">
                <div className="d-inline-block">
                    <h6>Daniel Madsen</h6>
                    <p>Good morning! Congratulations Friend...</p>
                </div>
                <div>
                    <span>20 Feb</span>
                    <span>3</span>
                </div>
            </div>{/* end media-body */}
        </a>
    )
}