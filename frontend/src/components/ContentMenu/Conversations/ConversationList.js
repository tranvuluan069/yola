import ConversationItem from "./ConversationItem";

export default function ConversationList() {
  return (
    <div>
      <div className="chat-body-left" data-simplebar>
        <div className="tab-content chat-list" id="pills-tabContent">
          <div className="tab-pane fade show active" id="general_chat">
            {/* conversation item */}
            <ConversationItem />
            {/* conversation item */}
          </div>
        </div>
      </div>
    </div>
  )
}
