import ChatHeader from './ChatHeader';
import ChatBody from './ChatBody';
import ChatFooter from './ChatFooter';
import Spinner from './../spinners/spinner';
import { useSelector } from 'react-redux';
import { useEffect, useState, useRef } from 'react';
import {decryptMessage, encryptMessage} from './../../utils/ConvertMessages';

export default function Chat(props) {
    const { socket } = props;
    const conversation = useSelector(state => state.conversation);
    const [sender, setSender] = useState();
    const [recipients, setRecipients] = useState();
    const account = useSelector(state => state.auth);
    const [listMessage, setListMessage] = useState([]);
    const [arrivalMessage, setArrivalMessage] = useState();

    useEffect(() => {
        // note init socket
        console.log('effect on listen' );
        socket && socket.off('server_send_message');
        socket && socket.on('server_send_message', data => {
            console.log('client recitrive');
            console.log(data);
            console.log('conversation id in data: ' + data.conversationId);
            console.log('conversation id in redux store: '+ conversation.conversation._id);
            if (data.conversationId === conversation.conversation._id) {
                console.log('server send data: ')
                console.log(data);
                setArrivalMessage(data);
            }
        });
    }, [conversation.conversation]);


    useEffect(() => {
        console.log(conversation.conversation._id);
        if (conversation.conversation) {
            const senderTemp = conversation.conversation.members.find(member => member.uid === account.account._id);
            const recipientArrayTemp = conversation.conversation.members;
            setSender(senderTemp);
            setRecipients(recipientArrayTemp);
        }
        setListMessage(conversation.messages);
    }, [conversation.conversation]);




    useEffect(() => {
        if (arrivalMessage) {
            setListMessage([...listMessage, arrivalMessage]);
        }
    }, [arrivalMessage]);

    return (
        <>
            <div>
                {/* Chat Header */}
                {sender && recipients && <ChatHeader recipients={recipients} sender={sender} />}
                {/* END Chat Header */}
                {/* Chat Body */}
                {sender && recipients && <ChatBody listMessage={listMessage} />}
                {/* END Chat Body */}
                {/* Chat Footer */}
                {sender && recipients && <ChatFooter setListMessage={setListMessage} listMessage={listMessage} socket={socket} sender={sender} recipients={recipients} />}
                {/* END Chat Footer */}
            </div>
            {conversation.isFetching && <Spinner />}
        </>
    )

}