import SendMessage from './MessageItem/SendMessage';
import ArrivalMessage from './MessageItem/ArrivalMessage';
import { decryptMessage } from '../../utils/ConvertMessages';
import { useSelector } from 'react-redux';
import { useState, useEffect } from 'react';

export default function ChatBody(props) {
    const { listMessage } = props;
    const account = useSelector(state => state.auth);


    return (
        <div className="chat-body" data-simplebar>
            <div className="chat-detail">
                {/* Message Item */}
                {listMessage?.map((value, key) => {
                    console.log(value);
                    let message;
                    let isSender = false;
                    value.text.map(item => {
                        if (item.uid == account.account._id) {
                            if (value.sender == account.account._id)
                                isSender = true;
                            message = decryptValue(item.encryptMessage, account.account.rsa.privateKey, value.date);
                        }
                    })
                    return isSender ?   (<SendMessage key={key} message={message} />) : (<ArrivalMessage key={key} message={message} />)
                    // console.log(value);
                    // if (value.recipients.find(recipient => recipient.uid === account.account._id)) {
                    //     return <ArrivalMessage key={key} message={decryptMessage(value.text.)} />
                    // } else {
                    //     return <SendMessage key={key} message={value} />
                    // }
                })
                }

                {/* ENd message Item */}
            </div>
        </div>
    )
}

function decryptValue(message, privateKey, date) {
    let decryptedMessage = decryptMessage(message, privateKey);
    return {
        text: decryptedMessage,
        date: date
    }
}
