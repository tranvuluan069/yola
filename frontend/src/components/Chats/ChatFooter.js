import { useState, useRef } from 'react';
import { sendMessage } from '../../apis/conversationApi';
import { useSelector, useDispatch } from 'react-redux';
import { encryptMessage } from '../../utils/ConvertMessages';

export default function ChatFooter(props) {
    const { sender, recipients, socket,setListMessage, listMessage } = props;
    // const {socket} = useRef(props);
    const account = useSelector(state => state.auth);
    const conversation = useSelector(state => state.conversation);
    const [textSend, setTextSend] = useState();
    const dispatch = useDispatch();

    const handleSendMessage = () => {
        const date = new Date();
        const arrayEncrypt = encryptMessage(textSend, recipients);
        const message = {
            conversationId: conversation.conversation._id,
            sender: sender.uid,
            recipients: recipients,
            text: arrayEncrypt,
            date: date
        }
        // // *note socket reload **** init socket
        socket && socket.emit("client_send_message", message);
        sendMessage(message, dispatch);
    }

    return (
        <div className="chat-footer">
            <div className="row">
                <div className="col-12 col-md-9">
                    <span className="chat-admin"><img src={account.account.image} alt="user" className="rounded-circle thumb-sm" /></span>
                    <form className="row">
                        <input onChange={(event) => setTextSend(event.target.value)} type="text" className="mx-3 form-control col-5" id="chatInput" placeholder="Type something here..." />
                        <button id="btnChat" type="reset" onClick={handleSendMessage} className="btn btn-primary mb-1 mx-3">Send</button>
                    </form>
                </div>{/* col-8 */}
                <div className="col-3">
                    <div className="d-none d-sm-inline-block chat-features">
                        <a><i className="fa fa-camera p-1" /></a>
                        <a><i className="fa fa-paperclip p-1" /></a>
                        <a><i className="fa fa-microphone p-1" /></a>
                    </div>
                </div>
            </div>
        </div>
    )
}