import dateFormat from 'dateformat';

export default function SendMessage(props) {
    const {message} = props;

    return (
        <div className="media">
            {/* message body_REVERSE (Arrival) */}
            <div className="media-body reverse">
                <div className="chat-msg">
                    <p>{message.text}</p>
                </div>
                <div className="chat-time">{ dateFormat(message.date, "dddd, h:MM:ss TT") }</div>
            </div>
            {/* END message body_REVERSE (Arrival) */}
        </div>
    )
}