import dateFormat from 'dateformat';

export default function ArrivalMessage(props) {
    const {message} = props;
    console.log(message);

    return (
        <div className="media">
            {/* message body_SEND */}
            <div className="media-body">
                <div className="chat-msg">
                    <p>{message.text}</p>
                </div>
                <div className="chat-time">{ dateFormat(message.date, "dddd, h:MM:ss TT") }</div>
            </div>
            {/* END message body_SEND */}
        </div>
    )
}