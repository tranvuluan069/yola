import {useState, useEffect} from 'react';

export default function ChatHeader(props) {
    let { recipients, sender } = props;

    let filterRecipients = recipients.filter(recipient => recipient.uid !== sender.uid);

    return (
        recipients ? (
        <div className="chat-header">
            <a className="media">
                <div className="media-left">
                    <img src={filterRecipients[0].avatar} alt="user" className="rounded-circle thumb-sm" />
                </div>
                <div className="media-body">
                    <div>
                        <h6 className="m-0">{filterRecipients[0].fullname}</h6>
                        <p className="mb-0">Last seen: 2 hours ago</p>
                    </div>
                </div>
            </a>
            <div className="chat-features">
                <div className="d-none d-sm-inline-block">
                    <a ><i className="la la-phone" /></a>
                    <a ><i className="la la-video" /></a>
                    <a ><i className="la la-trash-alt" /></a>
                    <a ><i className="la la-ellipsis-v" /></a>
                </div>
            </div>
        </div>) : (<></>)
    )
}