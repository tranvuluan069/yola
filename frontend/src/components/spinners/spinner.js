export default function Spinner() {

    return (
        <div className="spinner-overlay ">
            <div className="container">
                <div className="row vh-100 justify-content-center spinner-loading">
                    <div className="text-center">
                        <div className="spinner-border">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}