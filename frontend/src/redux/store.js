import { configureStore, combineReducers } from '@reduxjs/toolkit';
import {
    persistStore, persistReducer, FLUSH,
    REHYDRATE,
    PAUSE,
    PERSIST,
    PURGE,
    REGISTER,
} from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import storageSession from 'redux-persist/lib/storage/session'

import AuthReducer from './authSlice';
import ContactReducer from './contactSlice';
import ConversationReducer from './conversationSlice';
import SocketReducer from './socketSlice';

const persistConfig = {
    key: 'root',
    storage: storageSession
}

const RootReducer = combineReducers({
    auth: AuthReducer,
    contact: ContactReducer,
    conversation: ConversationReducer,
    socket: SocketReducer
});

const PersistedReducer = persistReducer(persistConfig, RootReducer);

export const store = configureStore({
    reducer: PersistedReducer,
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: {
                ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
            },
        })
});

export let persistor = persistStore(store);