import { createSlice } from '@reduxjs/toolkit';

const conversationSlice = createSlice({
    name: 'conversation',
    initialState: {
        conversation: null,
        messages: null,
        isFetching: false,
        error: false
    },

    reducers: {
        getConversationStart: (state) => {
            state.isFetching = true;
            state.error = false;
        },
        getConversationSuccess: (state, action) => {
            state.isFetching = false;
            state.error = false;
            state.conversation = action.payload.conversation;
            state.messages = action.payload.messages;
        },
        getConversationFailure: (state) => {
            state.isFetching = false;
            state.error = true;
        },

        sendMessageStart: (state) => {
            state.isFetching = true;
            state.error = false;
        },
        sendMessageSuccess: (state, action) => {
            state.isFetching = false;
            state.error = false;
        },
        sendMessageFailure: (state) => {
            state.sendMessage.isFetching = false;
            state.sendMessage.error = true;
        }
    }
})

export const {
    getConversationStart,
    getConversationSuccess,
    getConversationFailure,

    sendMessageStart,
    sendMessageSuccess,
    sendMessageFailure
} = conversationSlice.actions;

export default conversationSlice.reducer;
