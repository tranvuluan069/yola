import { createSlice } from '@reduxjs/toolkit';

const contactSlice = createSlice({
    name: 'contact',
    initialState: {
        contacts: null,
        getContacts: {
            isFetching: false,
            error: false
        }
    },

    reducers: {
        getContactsStart: (state) => {
            state.getContacts.isFetching = true;
            state.getContacts.error = false;
        },
        getContactsSuccess: (state, action) => {
            state.getContacts.isFetching = false;
            state.getContacts.error = false;
            state.contacts = action.payload;
        }, 
        getContactsFailure: (state) => {
            state.getContacts.isFetching = false;
            state.getContacts.error = true;
        }
    }
})

export const {
    getContactsStart,
    getContactsSuccess,
    getContactsFailure
} = contactSlice.actions;

export default contactSlice.reducer;
