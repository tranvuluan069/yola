import { createSlice } from '@reduxjs/toolkit';

const authSlice = createSlice({
    name: 'auth',
    initialState: {
        account: null,
        accessToken: null,
        login: {
            step: 1,
            isFetching: false,
            error: false
        },

        logout: {
            isFetching: false,
            error: false
        },

        register: {
            step: 1,
            isFetching: false,
            user: null,
            error: false,
            message: null
        },

        auth2F: {
            isFetching: false,
            error: false
        },
        forgotPassword: {
            isFetching: false,
            error: false,
            step: 1,
            username: null
        }

    },

    reducers: {
        loginStart: (state, action) => {
            state.login.isFetching = true;
            state.login.error = false;
        },
        loginSuccess: (state, action) => {
            state.login.isFetching = false;
            state.login.error = false;
            state.account = action.payload.account;
            state.accessToken = action.payload.accessToken;
            state.login.step = 1;
        },
        loginFailure: (state) => {
            state.login.isFetching = false;
            state.login.error = true;
        },

        loginRecieSMSOTP: (state, action) => {
            state.login.step = action.payload.step;
            state.login.isFetching = false;
            state.login.error = false;
        },

        stepRegis1Start: (state) => {
            state.register.isFetching = true;
            state.register.error = false;
        },
        stepRegis1Success: (state, action) => {
            state.register.isFetching = false;
            state.register.error = false;
            state.register.user = action.payload.user;
            state.register.step = action.payload.step;
        },
        stepRegis1Failure: (state, action) => {
            state.register.isFetching = false;
            state.register.error = true;
            state.register.step = 1;
            state.register.message = action.payload.message;
        },

        removeUserRegister: (state) => {
            state.register.isFetching = false;
            state.register.error = false;
            state.register.user = null;
        },

        refreshToken: (state, action) => {
            state.accessToken = action.payload.accessToken;
        },

        logoutStart: (state) => {
            state.logout.isFetching = true;
            state.logout.error = false;
        },

        logoutSuccess: (state) => {
            state.logout.isFetching = false;
            state.logout.error = false;
            state.account = null;
            state.accessToken = null;
            sessionStorage.clear();
        },

        logoutFailure: (state) => {
            state.logout.isFetching = false;
            state.logout.error = true;
        },

        stepRegister: (state, action) => {
            state.register.step = action.payload.step;
        },

        stepLogin: (state, action) => {
            state.login.step = action.payload.step;
        },

        forgotPasswordStart: (state) => {
            state.forgotPassword.isFetching = true;
            state.forgotPassword.error = false;
        },
        forgotPasswordSuccess: (state) => {
            state.forgotPassword.isFetching = false;
            state.forgotPassword.error = false;
        },
        forgotPasswordFailure: (state) => {
            state.forgotPassword.isFetching = false;
            state.forgotPassword.error = true;
        },
        setStepForgot: (state, action) => {
            state.forgotPassword.step = action.payload.step;
        },
        setUserForgot: (state, action) => {
            state.forgotPassword.username = action.payload.username;
            state.forgotPassword.phone = action.payload.phone;
        },
        refreshToken: (state, action) => {
            state.accessToken = action.payload;
        },
        setUserProfile: (state, action) => {
            state.account = action.payload.account;
        }

    }
})

export const {
    loginStart,
    loginSuccess,
    loginFailure,

    stepRegis1Start,
    stepRegis1Success,
    stepRegis1Failure,

    stepRegister,
    stepLogin,

    loginRecieSMSOTP,
    logoutSuccess,
    logoutStart,
    logoutFailure,
    removeUserRegister,
    forgotPasswordStart,
    forgotPasswordSuccess,
    forgotPasswordFailure,
    setStepForgot,
    setUserForgot,
    refreshToken,
    setUserProfile
} = authSlice.actions;

export default authSlice.reducer;