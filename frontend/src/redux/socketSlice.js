import { createSlice } from '@reduxjs/toolkit';

const socketSlice = createSlice({
    name: 'socket',
    initialState: {
        socket: null,
        isFetching: false,
        error: false
    },

    reducers: {
        connectSocket: (state, action) => {
            state.socket = action.payload;
            console.log('run this');
        }
    }
})

export const {
    connectSocket
} = socketSlice.actions;

export default socketSlice.reducer;
