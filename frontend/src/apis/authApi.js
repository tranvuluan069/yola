import axios from 'axios';
import {
    loginStart, loginSuccess, loginFailure,
    stepRegis1Start, stepRegis1Success, stepRegis1Failure,
    logoutStart, logoutSuccess, logoutFailure,
    loginRecieSMSOTP, removeUserRegister
} from '../redux/authSlice';

import { createRSA, decryptPrivateKey } from './../utils/rsa';

import {CreateAxiosJWT} from './createInstance';


export const registerStep1 = async (user, dispatch) => {
    dispatch(stepRegis1Start());
    try {
        const res = await axios.post('/auth/register', { username: user.username });
        dispatch(stepRegis1Success({ user: user, step: 2 }));

    } catch (err) {
        console.log(err.response.data);
        dispatch(stepRegis1Failure(err.response.data));
    }
}

export const registerStep2 = async (userOTP, dispatch) => {
    dispatch(stepRegis1Start());
    try {
        const RSA = createRSA(userOTP.password);
        userOTP = { ...userOTP, rsa: RSA };
        const res = await axios.post('/auth/verifyRegister', userOTP);
        dispatch(removeUserRegister());

    } catch (err) {
        console.log('Loi');
        console.log(err);
        dispatch(stepRegis1Failure({ message: err.response.message }));
    }
}

export const getSmsOTP = async (user, dispatch) => {
    try {
        let sendOTP = await axios.post('/auth/login', user);
        dispatch(loginRecieSMSOTP({ step: 2 }));

    } catch (err) {
        console.log(err);
        dispatch(loginFailure());

    }

}

export const verifySmsOTP = async (user, dispatch, navigate) => {
    try {
        dispatch(loginStart());
        let verifyOTP = await axios.post('/auth/verifyLogin', user);
        let userAccount = verifyOTP.data;
        userAccount.account.rsa.privateKey = decryptPrivateKey(user.password, userAccount.account.rsa.privateKey);
        dispatch(loginSuccess({
            account: userAccount.account,
            accessToken: userAccount.accessToken
        }));
        navigate('/');
    } catch (err) {
        console.log(err);
        dispatch(loginFailure());
    }
}


export const logout = (dispatch, navigate) => {
    dispatch(logoutStart());
    dispatch(logoutSuccess());
    //     axios.post('/auth/logout')
    //         .then(res => {
    //             dispatch(logoutSuccess({step: 1}));
    //             navigate('/login');
    //         })
    //         .catch(err => {
    //             dispatch(logoutFailure());
    //         });
}

export const getPrivateKeyByUid = async (uid) => {
    try {
        const data = await axios.post('/auth/getPrivateKey', { uid: uid });
        return data.data;
    } catch (err) {
        console.log(err);
    }
}

export const forgotPasswordSendOTP = async (userOTP) => {
    try {
        const data = await axios.post('/auth/forgotpassword', { phone: userOTP.phone, username: userOTP.username });
        return data;
    } catch (err) {
        // console.log(err);
        return false;
    }
}


export const verifyForgotPasswordOTP = async (userOTP) => {
    try {
        console.log(userOTP);
        const data = await axios.post('/auth/verifyForgotPassword', userOTP);
        return data;
    } catch (err) {
        console.log(err.response.message);
        return false;
    }
}


export const resetForgotPassword = async (user) => {
    try {
        const data = await axios.post('/auth/resetPassword', user);
        return data;
    } catch(err) {
        console.log(err.response.message);
        return false;
    }
}

export const userProfile = async (accessToken, userId) => {
    try {
        const AxiosJWT  = await CreateAxiosJWT(accessToken);
        const data = await AxiosJWT.post('/auth/userProfile', { 
            userId: userId
        }, {
            headers: {
                'Authorization': `Bearer ${accessToken}`
            }
        });
        return data;
    } catch(err) {
        console.log(err.response.message);
        return false;
    }
}