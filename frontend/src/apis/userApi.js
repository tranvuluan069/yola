import axios from 'axios';
import { CreateAxiosJWT } from './createInstance';

export const getUserById = async (userId, accessToken,dispatch) => {
    try {
        const AxiosJWT  = await CreateAxiosJWT(accessToken, dispatch);
        const fetchUser = await AxiosJWT.get(`/user/getUser?_id=${userId}`);
        return fetchUser.data;
    } catch (error) {
        console.log(error);
    }
}

export const getUsersByArrayId = async (arrayId) => {
    try {
        const users = await axios.post('/user/getUsersByArrayId', { array: arrayId });
        return users.data;
    } catch (error) {
        console.log(error);
        return false;
    }

}