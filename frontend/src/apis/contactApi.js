import axios from 'axios';
import { getUsersByArrayId } from './userApi';
import {
    getContactsStart, getContactsSuccess, getContactsFailure
} from './../redux/contactSlice';

import {CreateAxiosJWT} from './createInstance';

export const getContacts = async (user, dispatch, accessToken) => {
    dispatch(getContactsStart());
    try {
        const AxiosJWT  = await CreateAxiosJWT(accessToken, dispatch);
        const contacts = await AxiosJWT.post('/contact', { _id: user._id });
        const arrId = contacts.data;
        const users = await getUsersByArrayId(arrId);
        dispatch(getContactsSuccess(users));
    } catch (error) {
        console.log(error);
        dispatch(getContactsFailure());
    }
}

export const searchContacts = async (user, setIsLoading) => {
    try {
        const users = await axios.get(`/user/searchUser/?q=${user}`).then(res => res.data);
        setIsLoading(false);
        return users;
    } catch (error) {
        console.log(error);
    }
}

export const addContact = async (data, dispatch, accessToken) => {
    try {
        console.log(data);
        const AxiosJWT  = await CreateAxiosJWT(accessToken, dispatch);
        const addfriend = await AxiosJWT.post(`/contact/add`, data);
    } catch(error) {
        console.log(error);
    }
}