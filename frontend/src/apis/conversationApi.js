import {
    getConversationStart, getConversationSuccess, getConversationFailure,
    sendMessageStart,
    sendMessageSuccess,
    sendMessageFailure
} from '../redux/conversationSlice';
import axios from 'axios';


export const getPrivateConversation = async (arrMemberId, dispatch) => {
    dispatch(getConversationStart());
    try {
        console.log('arraymemberId');
        console.log(arrMemberId);
        const fetchPrivateCon = await axios.post('/conversation/getPrivateConversation', {
            arrayMemberId: arrMemberId
        });
        const privateConversation = fetchPrivateCon.data;
        console.log('client get private conversation');
        console.log(privateConversation);
        if (privateConversation.length == 0) {
            console.log('create conversation')
            const privateConversation = await createPrivateConversation(arrMemberId);
            console.log('client create conversation');
            console.log(privateConversation);
            dispatch(
                getConversationSuccess({
                    conversation: privateConversation,
                    messages: []
                })
            )
        } else {
            const messages = await getMessages(privateConversation._id);
            console.log('get messages in conversation');
            console.log(messages);
            dispatch(getConversationSuccess({
                conversation: privateConversation,
                messages: messages
            }));
        }
    } catch (error) {
        console.log(error);
        dispatch(getConversationFailure());
    }
}

export const createPrivateConversation = async (arrMemberId, dispatch) => {
    try {
        const createPrivateCon = await axios.post('/conversation/createPrivateConversation', {
            arrayMemberId: arrMemberId
        });
        return createPrivateCon.data;
    } catch (error) {
        console.log(error);
        return false;
    }
}


export const getMessages = async (conversationId) => {
    try {
        const fetchMessage = await axios.get(`/message/getMessageByConversationId?conversationId=${conversationId}`);
        const messages = fetchMessage.data;
        return messages;
    } catch (error) {
        console.log(error);
    }
}

export const sendMessage = async (message, dispatch) => {
    dispatch(sendMessageStart());
    try {
        const sendMessage = await axios.post(`/message/add`, message).then(res => res.data);
        dispatch(sendMessageSuccess());
    } catch (error) {
        console.log(error);
        dispatch(sendMessageFailure());
    }
}
