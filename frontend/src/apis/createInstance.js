import axios from 'axios';
import jwt_decode from 'jwt-decode';
import { refreshToken } from './../redux/authSlice';
import Cookies from 'js-cookie';


export  const CreateAxiosJWT = (accessToken, dispatch) => {
  const newIntance = axios.create();
  newIntance.interceptors.request.use(
    // return config
    async (config) => {
      const decodedToken = jwt_decode(accessToken);
      if (decodedToken.exp < Date.now() / 1000) {
        const resAPI = await axios.post('/auth/refreshToken', {
          refreshToken: Cookies.get('refreshToken')
        });
        dispatch(refreshToken(resAPI.data.accessToken));
        Cookies.set('refreshToken', resAPI.data.refreshToken);
        config.headers.token = `Bearer ${resAPI.data.accessToken}`;
        console.log('refresh token');
      } else {
        config.headers.token = `Bearer ${accessToken}`;
        console.log('current access token' + accessToken);
      }
      return config;
    }, (error) => {
      return Promise.reject(error);
    }
  );
  return newIntance;
}
