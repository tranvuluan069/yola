const express = require('express');
const app = express();
const https = require('https');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser')
const dotenv = require('dotenv');
const morgan = require('morgan');
const cors = require('cors');
const path = require('path');
const fs = require('fs')
dotenv.config({ path: 'config.env' });
const PORT = process.env.PORT || 5678
const connectDB = require('./server/databases/connection');

// Certificate
const privateKey = fs.readFileSync('./ssl/key.pem', 'utf8');
const certificate = fs.readFileSync('./ssl/cert.pem', 'utf8');

const credentials = {
    key: privateKey,
    cert: certificate
}

// Router
const authRouter = require('./server/routes/authRouter');
const contactRouter = require('./server/routes/contactRouter');
const conversationRouter = require('./server/routes/conversationRouter');
const messageRouter = require('./server/routes/messageRouter');
const userRouter = require('./server/routes/userRouter');

// create a write stream (in append mode)
var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
// setup the logger
app.use(morgan('combined', { stream: accessLogStream }));

connectDB();
app.use(cookieParser());
app.use((req, res, next) => {
    const allowedOrigins = ["http://localhost:3000"];
    const origin = req.headers.origin;
    if (allowedOrigins.includes(origin)) {
        res.setHeader('Access-Control-Allow-Origin', origin);
    }
    res.header('Access-Control-Allow-Methods', 'GET, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.header('Access-Control-Allow-Credentials', true);
    return next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


app.use('/auth', authRouter);
app.use('/contact', contactRouter);
app.use('/conversation', conversationRouter);
app.use('/message', messageRouter);
app.use('/user', userRouter);

app.use((error, req, res, next) => {
    console.log(error);
    const status = error.statusCode || 500;
    const message = error.message;
    const data = error.data;
    res.status(status).json({ message: message, data: data });
});


const httpsServer = https.createServer(credentials, app);
httpsServer.listen(PORT, () => console.log(`Server is running on port ${PORT}`)); 