const mongoose = require('mongoose');

const RsaSchema = mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    publicKey: {
        type: String,
        require: true
    },
    privateKey: {
        type: String,
        reuqire: true
    }
});

module.exports = new mongoose.model('Rsa', RsaSchema);