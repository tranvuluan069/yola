const mongoose = require('mongoose');

const messageSchema = new mongoose.Schema({
    conversationId: {
        type: String,
    },
    sender: {
        type: String,
    },
    recipients: {
        type: Array
    },
    text: {
        type: Array,
    },
    date: {
        type: Date
    }
}, { timestamp: true });

module.exports = mongoose.model('message', messageSchema);