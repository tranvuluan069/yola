const mongoose = require('mongoose');

const accountSchema = new mongoose.Schema({
    fullname: {
        type: String,
        require: true
    },
    email: {
        type: String,
        required: true
    },
    username: {
        type: String,
        reuqired: true
    },
    password: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        require: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    permission: {
        type: String,
        default: 'user'
    },
    friends: {
        type: Array,
        default: []
    },
    image: {
        type: String
    },
    rsa: {
        type: Object
    }
    // rsa: { publicKey: String, privateKey: String }
}, {timestamps: true});


module.exports = mongoose.model('account', accountSchema);