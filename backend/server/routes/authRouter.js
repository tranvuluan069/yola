const router = require('express').Router();
const authController = require('./../controllers/authController');
const AuthOTPMiddleware = require('../controllers/middleware/verifyAuth');
const AccountModel = require('../models/accountModel');
const { body } = require('express-validator')



router.post('/register',
    [
        body('email')
            .isEmail()
            .withMessage('Please enter a valid email.')
            .normalizeEmail(),
        body('username')
            .custom((value, { req }) => {
                console.log('value', value)
                return AccountModel.findOne({ username: value })
                    .then(userDoc => {
                        if (userDoc) {
                            return Promise.reject('Username already in use')
                        }
                    })
            })
    ], AuthOTPMiddleware.registerSendSMSOTP);

router.post('/verifyRegister', [
    body('otp'),
    body('username')
], AuthOTPMiddleware.registerVerifySMSOTP, authController.postRegister);

router.post('/forgotpassword', [
    body('email')
        .isEmail()
        .withMessage('Please enter a valid email.'),
    body('username')
        .custom((value, { req }) => {
            return AccountModel.findOne({ username: value })
                .then(userDoc => {
                    if (!userDoc) {
                        return Promise.reject('Username not exists')
                    }
                })
        })

], AuthOTPMiddleware.forgotPasswordSendSMSOTP);


router.post('/login', [
    body('username')
        .isLength({ min: 1 }),
    body('password')
        .isLength({ min: 2, max: 50 })
        .trim()
        .withMessage('Password has to be valid.')
], authController.checkExistUser, AuthOTPMiddleware.authSendSMSOTP);

router.post('/verifyLogin', [
    body('username')
        .isLength({ min: 1 }),
    body('otp')
        .isLength({ min: 1 })
], AuthOTPMiddleware.verifySMSOTP, authController.postLogin);
// router.get('/signin', controller.getSignIn);


router.post('/verifyForgotPassword', AuthOTPMiddleware.forgotVerifySMSOTP);
router.post('/resetPassword', authController.postResetPassword);

router.post('/refreshToken', authController.postRefreshToken);



router.post('/logout', authController.postLogout);


module.exports = router;