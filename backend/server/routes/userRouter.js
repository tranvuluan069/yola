const router = require('express').Router();
const userController = require('./../controllers/userController');
const VerifyToken = require('../controllers/middleware/verifyToken');


router.get('/', userController.getAll);

router.get('/getUser', VerifyToken.verifyToken, userController.getById);

router.post('/getUsersByArrayId', userController.getByArrayId);

router.get('/searchUser', userController.searchUser);

module.exports = router;