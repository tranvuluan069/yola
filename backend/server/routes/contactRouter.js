const router = require('express').Router();
const contactController = require('./../controllers/contactController');
const middleware = require('./../controllers/middleware/verifyToken');

router.post('/', middleware.verifyToken,contactController.getContacts);

router.post('/add', middleware.verifyToken, contactController.addContact);


module.exports = router;