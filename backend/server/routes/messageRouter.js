const router = require('express').Router();
const messageController = require('./../controllers/messageController');

router.post('/add', messageController.addNewMessage);

router.get('/:conversationId', messageController.getMessageByConversationId);


module.exports = router;