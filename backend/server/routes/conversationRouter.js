const router = require('express').Router();
const conversationController = require('./../controllers/conversationController');
const middleware = require('./../controllers/middleware/verifyToken');


router.post("/getPrivateConversation", middleware.verifyToken, conversationController.getPrivateConversation);

router.post('/createPrivateConversation', middleware.verifyToken, conversationController.createPrivateConversation);

// router.post('/find/:firstUserId/:secondUserId', conversationController.getPrivateConversation);


module.exports = router;