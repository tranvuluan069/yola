const jwt = require('jsonwebtoken');

exports.verifyToken = (req, res, next) => {
    const token = req.headers.token;
    console.log(token);
    if (token) {
        const accessToken = token.split(" ")[1];
        console.log('veryfile token: ' + accessToken);
        jwt.verify(accessToken, process.env.JWT_ACCESS_TOKEN, (err, account) => {
            if (err) {
                console.log(err);
                res.status(403).json({
                    message: 'Token is invalid'
                })
            } else {
                console.log(account);
                req.account = account;
                console.log('Account: \n' + account + '\n -----');
                console.log('Passed');
                next();
            }
        })
    } else {
        res.status(403).json({ message: "You are not authentication" });
    }
}