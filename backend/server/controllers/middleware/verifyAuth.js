const AccountModel = require('../../models/accountModel');
const { validationResult } = require('express-validator')

let loginSMSOTPUsers = [];
let registerSMSOTPUsers = [];
let forgotSMSOTPUsers = [];

exports.authSendSMSOTP = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const error = new Error('Validation failed.');
            error.statusCode = 422;
            error.data = errors.array();
            throw error;
        }
        let otp = Math.floor(Math.random() * 100000);

        const accountSid = process.env.TWILIO_ACCOUNT_SID;
        const authToken = process.env.TWILIO_AUTH_TOKEN;
        const client = require('twilio')(accountSid, authToken);

        console.log(req.body);
        const userOTP = {
            otp: otp,
            username: req.body.username,
            exp: Date.now() + 60000
        }

        loginSMSOTPUsers = loginSMSOTPUsers.filter(item => item.username !== userOTP.username);
        loginSMSOTPUsers.push(userOTP);
        console.log('OTP Array in Server');
        console.log(loginSMSOTPUsers);
        const sms = 'Your code: ' + otp;

        client.messages
            .create({
                body: sms,
                messagingServiceSid: 'MG7f4ad9c0d474d021441122ec59cbac22',
                to: '+84868486575'
            })
            .then(message => {
                console.log(message);
            })
            .done();

        res.status(200).json({ "message": "OTP has been sent" });
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }

}


exports.verifySMSOTP = (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const error = new Error('Validation failed.');
            error.statusCode = 422;
            error.data = errors.array();
            throw error;
        }
        const userOTP = {
            username: req.body.username,
            otp: req.body.otp,
            exp: req.body.exp
        }

        console.log(userOTP);
        const result = loginSMSOTPUsers.find(item => item.username == userOTP.username && item.otp == userOTP.otp && userOTP.exp < item.exp);

        if (result) {
            const result = loginSMSOTPUsers.find(item => item.username == userOTP.username && item.otp == userOTP.otp && userOTP.exp < item.exp);
            next();
        } else {
            res.status(403).json({ message: 'OTP invalid' });
        }
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
}



exports.registerSendSMSOTP = async (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const error = new Error('Validation failed.');
            error.statusCode = 422;
            error.data = errors.array();
            throw error;
        }
        let otp = Math.floor(Math.random() * 100000);

        const accountSid = process.env.TWILIO_ACCOUNT_SID;
        const authToken = process.env.TWILIO_AUTH_TOKEN;
        const client = require('twilio')(accountSid, authToken);

        console.log(req.body);
        const userOTP = {
            otp: otp,
            username: req.body.username,
            exp: Date.now() + 60000
        }

        registerSMSOTPUsers = registerSMSOTPUsers.filter(item => item.username !== userOTP.username);
        registerSMSOTPUsers.push(userOTP);
        console.log('OTP Array in Server');
        console.log(registerSMSOTPUsers);
        const sms = 'Your code: ' + otp;

        client.messages
            .create({
                body: sms,
                messagingServiceSid: 'MG7f4ad9c0d474d021441122ec59cbac22',
                to: '+84868486575'
            })
            .then(message => {
                console.log(message);
            })
            .done();

        res.status(200).json(userOTP);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
}

exports.registerVerifySMSOTP = (req, res, next) => {
    try {
         const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const error = new Error('Validation failed.');
            error.statusCode = 422;
            error.data = errors.array();
            throw error;
        }
        const userOTP = {
            username: req.body.username,
            otp: req.body.otp,
            exp: req.body.exp
        }

        console.log(userOTP);
        const result = registerSMSOTPUsers.find(item => item.username == userOTP.username && item.otp == userOTP.otp && userOTP.exp < item.exp);

        if (result) {
            const result = registerSMSOTPUsers.find(item => item.username == userOTP.username && item.otp == userOTP.otp && userOTP.exp < item.exp);
            next();
        } else {
            res.status(403).json({ message: 'OTP invalid' });
        }
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
}


/// Forgot Password


exports.forgotPasswordSendSMSOTP = async (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const error = new Error('Validation failed.');
            error.statusCode = 422;
            error.data = errors.array();
            throw error;
        }
        let otp = Math.floor(Math.random() * 100000);

        const accountSid = process.env.TWILIO_ACCOUNT_SID;
        const authToken = process.env.TWILIO_AUTH_TOKEN;
        const client = require('twilio')(accountSid, authToken);

        console.log(req.body);
        const userOTP = {
            otp: otp,
            username: req.body.username,
            exp: Date.now() + 60000
        }

        forgotSMSOTPUsers = forgotSMSOTPUsers.filter(item => item.username !== userOTP.username);
        forgotSMSOTPUsers.push(userOTP);
        console.log('OTP Array in Server');
        console.log(forgotSMSOTPUsers);
        const sms = 'Your code: ' + otp;

        // client.messages
        //     .create({
        //         body: sms,
        //         messagingServiceSid: 'MG7f4ad9c0d474d021441122ec59cbac22',
        //         to: '+84868486575'
        //     })
        //     .then(message => {
        //         console.log(message);
        //     })
        //     .done();

        res.status(200).json(userOTP);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
}


exports.forgotVerifySMSOTP = (req, res, next) => {
    try {
        const userOTP = {
            username: req.body.username,
            otp: req.body.otp,
            exp: req.body.exp
        }

        console.log(userOTP);

        const result = forgotSMSOTPUsers.find(item => item.username == userOTP.username && item.otp == userOTP.otp && userOTP.exp < item.exp);
        if (result) {
            res.status(200).json({ message: 'Successfully' });

        } else {
            res.status(403).json({ message: 'OTP invalid' });
        }
    } catch (err) {
        console.log(err)
        res.status(500).json({ message: "Server error" });
    }
}
