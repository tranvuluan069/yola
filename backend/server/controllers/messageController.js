const MessageModel = require('./../models/MessageModel');

exports.addNewMessage = (req, res) => {
    const newMessage = new MessageModel(req.body);
    newMessage.save()
            .then(data => {
                console.log(data);
                res.json(data);
            })
            .catch(err =>  res.status(500).json(err));
}

exports.getMessageByConversationId = (req, res) => {
    MessageModel.find({ 
        conversationId: req.query.conversationId
    })
    .then(data => {
        console.log(data);
        res.json(data);
    })
    .catch(err => res.status(500).json(err));
}

