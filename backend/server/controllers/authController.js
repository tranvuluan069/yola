const AccountModel = require('../models/accountModel');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const {validationResult} = require('express-validator');

let refreshTokens = [];



// Generate AccessToken
const generateAccessToken = (account) => {
    return jwt.sign({
        id: account.id,
    }, process.env.JWT_ACCESS_TOKEN, { expiresIn: '1h' });
}

// Generate RefreshToken
const generateRefreshToken = (account) => {
    return jwt.sign({
        id: account.id,
    }, process.env.JWT_REFRESH_TOKEN, { expiresIn: '5d' });
}

exports.checkExistUser = async (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const error = new Error('Validation failed.');
            error.statusCode = 422;
            error.data = errors.array();
            throw error;
        }
        const user = await AccountModel.findOne({ 'username': req.body.username });
        if (user) {
            let login = async () => {
                const checkPassword = await bcrypt.compare(req.body.password, user.password);
                if (checkPassword) {
                    next();
                } else {
                    const error = new Error('Password not match.');
                    error.statusCode = 404;
                    throw error;
                }
            }
            login();
        } else {
            const error = new Error('User not exist.');
            error.statusCode = 404;
            throw error;
        }
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }

}


exports.postLogin = (req, res) => {
    AccountModel.findOne({ username: req.body.username })
        .then(data => {
            if (data) {
                const accessToken = generateAccessToken(data);

                const refreshToken = generateRefreshToken(data);
                refreshTokens.push(refreshToken);
                //set Cookie
                res.cookie('refreshToken', refreshToken, {
                    httpOnly: true,
                    secure: false,
                    sameSite: "strict"
                });

                const { password, ...others } = data._doc;
                console.log(others);
                res.json({
                    message: 'Login success',
                    account: { ...others },
                    accessToken: accessToken,
                });
            } else {
                res.status(404).json({ message: 'Username is incorrect' });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ message: "Server Error" });
        })
}

exports.postRefreshToken = (req, res) => {
    console.log('------START refresh token -----')
    const refreshToken = req.cookies.refreshToken;
    console.log('Token Array In Server: ' + refreshTokens);
    console.log('Token In Client: ' + refreshToken);
    if (!refreshToken) {
        return res.status(401).json({ message: 'You are not authenticated' });
    }
    if (!refreshTokens.includes(refreshToken))
        return res.status(403).json({ message: 'Fobiden: Refresh token is not valid' });

    jwt.verify(refreshToken, process.env.JWT_REFRESH_TOKEN, (err, account) => {
        if (err)
            console.log(err);
        // Create nrew accessToken, refresToken
        refreshTokens = refreshTokens.filter(token => token !== refreshToken);
        const newAccessToken = generateAccessToken(account);
        const newRefreshToken = generateRefreshToken(account);
        refreshTokens.push(newRefreshToken);
        res.cookie('refreshToken', newRefreshToken, {
            httpOnly: true,
            secure: false,
            sameSite: "strict"
        });
        res.json({
            accessToken: newAccessToken,
            refreshToken: newRefreshToken,
        });
    })
    console.log('------END refresh token -----')
}


exports.postLogout = (req, res) => {
    res.clearCookie('refreshToken');
    refreshTokens = refreshTokens.filter(token => token !== req.cookies.refreshToken);
    console.log('------RefreshToken Array In Server ------');
    console.log(refreshTokens);
    res.json({ message: 'Logout successfully!' });
}


// Register
exports.postRegister = (req, res) => {
    let saveUser = async () => {
        const saltPassword = await bcrypt.genSalt(10);
        const securePassword = await bcrypt.hash(req.body.password, saltPassword);

        console.log(req.body);
        const account = new AccountModel({
            fullname: req.body.fullname,
            username: req.body.username,
            email: req.body.email,
            password: securePassword,
            phone: req.body.phone,
            image: req.body.image,
            rsa: req.body.rsa,
        });


        account.save()
            .then(data => {
                res.json(data);
            })
            .catch(err => {
                res.json({ message: err })
            })
    }
    saveUser();
}


// reset password
exports.postResetPassword = async (req, res) => {
    try {
        const saltPassword = await bcrypt.genSalt(10);
        const securePassword = await bcrypt.hash(req.body.password, saltPassword);
        const user = await AccountModel.findOne({ 'username': req.body.username });
        user.password = securePassword;
        await user.save();
        res.json({ message: 'Reset password successfully!' });
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: "Server Error" });
    }

}

