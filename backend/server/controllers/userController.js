const AccountModel = require('./../models/accountModel');


exports.getAll = async (req, res) => {
    try {
    const accountUsers = await AccountModel.find();
    res.json(accountUsers);
    } catch (err) {
        console.log(err);
        res.status(500).json(err);
    }
}


exports.getById = async (req, res) => {
    try {
        const user = await AccountModel.findOne({"_id": req.query._id});
        res.json(user);
    } catch (error) {
        console.log(error);
        res.status(500).json({message: 'Server error!'});
    }
}


exports.getByArrayId = async (req, res) => {
    try {
        const arrayId = req.body.array;
        const users = await AccountModel.find({
            "_id" : {
                $in: arrayId
            }
        });
        res.json(users);
    } catch (error) {
        console.log(error);
        res.status(500).json({message: 'Server error!'});
    }
}


exports.searchUser = async (req, res) => {
    try {
        const query = req.query.q;
        const users = await AccountModel.find({
            $or: [
                {fullname: { $regex: `^${query}`, $options: 'i' }},
                { phone: { $regex: `^${query}`, $options: 'i' } }
            ]
        });
        res.json(users);
    } catch (error) {
        console.log(error);
        res.status(500).json({message: 'Server error!'});
    }
}