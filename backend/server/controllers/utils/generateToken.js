const jwt = require('jsonwebtoken');

exports.generateToken = {
    // accessToken
    accessToken: (userId, email) => {
        const accessToken = jwt.sign({
            userId: userId,
            email: email
        }, process.env.JWT_ACCESS_TOKEN, { expiresIn: '30m' });
        return accessToken;
    },

    // refreshToken
    refreshToken: (userId, email) => {
        const refreshToken = jwt.sign({
            userId: userId,
            email: email
        }, process.env.JWT_REFRESH_TOKEN, { expiresIn: '1d' });
        return accessToken;
    }

}
