// Import the functions you need from the SDKs you need
const { initializeApp } = require("firebase/app");
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyA1q_At0h9fQdgJVRIhe5elV0CHoOfuTsc",
  authDomain: "auth-99.firebaseapp.com",
  projectId: "auth-99",
  storageBucket: "auth-99.appspot.com",
  messagingSenderId: "400805115551",
  appId: "1:400805115551:web:ad6a60f8d84d3f834454aa",
  measurementId: "G-H8ZX2NF0SQ"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
module.exports = {app, firebaseConfig};