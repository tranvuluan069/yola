const ConversationModel = require('./../models/conversationModel');
const AccountModel = require('./../models/accountModel');

exports.createPrivateConversation = async (req, res) => {
    const { arrayMemberId } = req.body;
    const membersData = []; // data save in conversation collection
    let arrayUser = await AccountModel.find({
        _id: {
            $in: arrayMemberId
        }
    });

    let pushArray = arrayUser.map(async (user) => {
        let data = {
            uid: user._id.toString(),
            avatar: user.image,
            fullname: user.fullname,
            publicKey: user.rsa.publicKey
        }
        membersData.push(data);
    });

    const promiseAll = await Promise.all(pushArray);
    const newConversation = new ConversationModel({
        "members": membersData
    });

    newConversation.save()
        .then((data) => {
            console.log('create conversation');
            console.log(data);
            res.json(data);
        })
        .catch(err => {
            res.status(500).json(err);
        });
}

exports.getPrivateConversation = async (req, res) => {
    try {
        const { arrayMemberId } = req.body;
        console.log(arrayMemberId);
        const conversations  = await ConversationModel.find();
        let filterConversation = conversations.filter(conversation => {
            return arrayMemberId.includes(conversation.members[0].uid) && arrayMemberId.includes(conversation.members[1].uid);
        });
        console.log(filterConversation[0]);
        res.json(filterConversation[0]);
    } catch (err) {
        res.status(500).json(err);
    }
   
}

