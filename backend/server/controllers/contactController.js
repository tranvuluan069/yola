const AccountModel = require('./../models/accountModel');

exports.getContacts = (req, res) => {
    // get friend list of account
    AccountModel.findOne({ "_id" : req.body._id})
        .then(data => {
            res.json(data.friends);
        })
        .catch(err => {console.log(err);  return res.status(500).json({ message: 'Server Error!' })});

}

exports.addContact = async (req, res) => {
    try {
        const currentAccount = req.body.currentAccount;
        const accountIdToAdd = req.body.accountIdToAdd;
        console.log('api add contact');
        console.log(req.body);
        let currentFriends = currentAccount.friends;
        let accountToAddFriends = accountIdToAdd.friends;
        currentFriends = currentFriends.filter(item => item !== accountIdToAdd._id);
        accountToAddFriends =  accountToAddFriends.filter(item => item !== currentAccount._id);
        currentFriends.push(accountIdToAdd._id); 
        accountToAddFriends.push(currentAccount._id);
        console.log('array friends current account');
        console.log(currentFriends);
        console.log('array feire  account to add');
        console.log(accountToAddFriends); 
        const updateCurrentAccount = await AccountModel.findOneAndUpdate({"_id": currentAccount}, {
            "$set": { "friends": currentFriends }
        })

        const updateAccountToAdd = await AccountModel.findOneAndUpdate({"_id": accountIdToAdd._id}, {
            "$set": { "friends": accountToAddFriends }
        })
        res.json('Successfully');
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: "Server error" });
    }
}
